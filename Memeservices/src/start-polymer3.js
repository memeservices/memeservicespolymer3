/**
 * @license
 * Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

// Import statements in Polymer 3.0 can now use package names.
// polymer-element.js now exports PolymerElement instead of Element,
// so no need to change the symbol. 

//Modified for Memeservices by Ramsey McGrath

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/paper-checkbox/paper-checkbox.js';
import { setPassiveTouchGestures } from '@polymer/polymer/lib/utils/settings';

class StartPolymer3 extends PolymerElement {
  static get properties () {
    return {
      message: {
        type: String,
        value: ''
      },
      memes: {
        type: Boolean,
        value: false,
        observer: 'toggleMemes'
      },
      loadComplete: {
        type: Boolean,
        value: false
      }
    };
  }

  constructor() {
    super();
    // Resolve warning about scroll performance 
    // See https://developers.google.com/web/updates/2016/06/passive-event-listeners
    setPassiveTouchGestures(true);
    this.message = 'The Home of Gourmet Memes';
  }

  ready(){
    // If you override ready, always call super.ready() first.
    super.ready();
    console.log(this.tagName);
  }
  
  toggleMemes(){
    if(this.memes && !this.loadComplete) {
      // See https://developers.google.com/web/updates/2017/11/dynamic-import
      import('./lazy-element.js').then((LazyElement) => {
        console.log("LazyElement loaded");
      }).catch((reason) => {
        console.log("LazyElement failed to load");
      });
      this.loadComplete = true;
    }
  }

  static get template () {
    return html`
      <style>
        paper-checkbox {
          --paper-checkbox-checked-ink-color: #FFFFFF;
          --paper-checkbox-unchecked-ink-color: #FFFFFF;
        }
      </style>

      <h1>Welcome to Memeservices</h1>
      <p>[[message]]</p>
      <paper-checkbox id="wantsmemes"
        toggles
        noink
        checked={{memes}}>I like memes.</paper-checkbox>
      <template is="dom-if" if=[[memes]]>
        <lazy-element><p>lazy loading...</p></lazy-element>
      </template>
    `;
  }
}

// Register the element with the browser.
customElements.define('start-polymer3', StartPolymer3);
